FROM docker 

RUN apk --update add py-pip \
    && apk add --virtual build-dependencies python-dev libffi-dev openssl-dev gcc libc-dev make \
    && pip install docker-compose \
    && apk del build-dependencies

